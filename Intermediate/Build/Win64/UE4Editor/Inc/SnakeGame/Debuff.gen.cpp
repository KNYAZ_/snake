// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Debuff.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDebuff() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ADebuff_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ADebuff();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ADebuff::StaticRegisterNativesADebuff()
	{
	}
	UClass* Z_Construct_UClass_ADebuff_NoRegister()
	{
		return ADebuff::StaticClass();
	}
	struct Z_Construct_UClass_ADebuff_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADebuff_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADebuff_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Debuff.h" },
		{ "ModuleRelativePath", "Debuff.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ADebuff_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ADebuff, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADebuff_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADebuff>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADebuff_Statics::ClassParams = {
		&ADebuff::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADebuff_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADebuff_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADebuff()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADebuff_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADebuff, 857030218);
	template<> SNAKEGAME_API UClass* StaticClass<ADebuff>()
	{
		return ADebuff::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADebuff(Z_Construct_UClass_ADebuff, &ADebuff::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ADebuff"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADebuff);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
