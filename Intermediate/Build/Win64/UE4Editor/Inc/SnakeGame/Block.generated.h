// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_Block_generated_h
#error "Block.generated.h already included, missing '#pragma once' in Block.h"
#endif
#define SNAKEGAME_Block_generated_h

#define SnakeGame_Source_SnakeGame_Block_h_16_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_Block_h_16_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_Block_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_Block_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABlock(); \
	friend struct Z_Construct_UClass_ABlock_Statics; \
public: \
	DECLARE_CLASS(ABlock, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABlock) \
	virtual UObject* _getUObject() const override { return const_cast<ABlock*>(this); }


#define SnakeGame_Source_SnakeGame_Block_h_16_INCLASS \
private: \
	static void StaticRegisterNativesABlock(); \
	friend struct Z_Construct_UClass_ABlock_Statics; \
public: \
	DECLARE_CLASS(ABlock, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ABlock) \
	virtual UObject* _getUObject() const override { return const_cast<ABlock*>(this); }


#define SnakeGame_Source_SnakeGame_Block_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABlock(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABlock) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlock); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlock(ABlock&&); \
	NO_API ABlock(const ABlock&); \
public:


#define SnakeGame_Source_SnakeGame_Block_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABlock(ABlock&&); \
	NO_API ABlock(const ABlock&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABlock); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABlock); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABlock)


#define SnakeGame_Source_SnakeGame_Block_h_16_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_Block_h_13_PROLOG
#define SnakeGame_Source_SnakeGame_Block_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Block_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Block_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_Block_h_16_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_Block_h_16_INCLASS \
	SnakeGame_Source_SnakeGame_Block_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_Block_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_Block_h_16_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_Block_h_16_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_Block_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Block_h_16_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_Block_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ABlock>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_Block_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
